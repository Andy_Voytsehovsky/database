package ua.od.fabrika.student.lessdatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import static ua.od.fabrika.student.lessdatabase.FeedReaderContract.FeedEntry.TABLE_NAME;

/**
 * Created by student on 6/3/2017.
 */

public class FeedReaderDbHelper extends SQLiteOpenHelper {

    private static FeedReaderDbHelper sInstance;

    public static final String CONTENT_AUTHORITY = "com.test.dblesson.provider";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "FeedReader.db";

    public static synchronized FeedReaderDbHelper getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new FeedReaderDbHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    private FeedReaderDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(FeedReaderContract.SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(FeedReaderContract.SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public static Uri CONTENT_URI = FeedReaderDbHelper.BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

}
