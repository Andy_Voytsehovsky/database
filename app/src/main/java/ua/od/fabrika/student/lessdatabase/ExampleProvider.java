package ua.od.fabrika.student.lessdatabase;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import static ua.od.fabrika.student.lessdatabase.FeedReaderContract.FeedEntry.TABLE_NAME;

public class ExampleProvider extends ContentProvider {

    FeedReaderDbHelper mDbHelper;

    private FeedReaderDbHelper feedReaderDbHelper;
    private static final UriMatcher URI_MATCHER = buildUriMatcher();

    //UriMatcher
    private static final int FEED_MATCH = 1;

    public static final int ENTITIES = 100;


    private static UriMatcher buildUriMatcher() {
        final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = FeedReaderDbHelper.CONTENT_AUTHORITY;
        uriMatcher.addURI(authority, TABLE_NAME, FEED_MATCH);
        return uriMatcher;
    }

    private String matchTable(Uri uri) {
        final int match = URI_MATCHER.match(uri);
        switch (match) {
            case FEED_MATCH:
                return FeedReaderContract.FeedEntry.TABLE_NAME;
            default:
                throw new UnsupportedOperationException("Table not matched: " + uri);

        }
    }

    @Override
    public boolean onCreate() {
        feedReaderDbHelper = FeedReaderDbHelper.getInstance(getContext());
        return true;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        String type = null;
        String tableName = matchTable(uri);
        final int match = URI_MATCHER.match(uri);
        switch (match) {
            case FEED_MATCH:
                type = "vnd.android.cursor.dir/vnd.dblesson.entity." + tableName;
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        return type;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        final SQLiteDatabase db = feedReaderDbHelper.getReadableDatabase();
        final String table = matchTable(uri);

        final SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(table);

        final Cursor cursor = builder.query(db, projection, selection, selectionArgs, null, null,
                sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = feedReaderDbHelper.getWritableDatabase();
        final String table = matchTable(uri);

        long rowId = db.insert(table, null, values);
        uri = ContentUris.withAppendedId(uri, rowId);
        getContext().getContentResolver().notifyChange(uri, null);
        return uri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = feedReaderDbHelper.getWritableDatabase();

        final String table = matchTable(uri);

        int retVal = db.delete(table, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return retVal;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = feedReaderDbHelper.getWritableDatabase();

        final String table = matchTable(uri);

        int retVal = db.update(table, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return retVal;
    }

    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {
        final SQLiteDatabase db = feedReaderDbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            final int numOperations = operations.size();
            final ContentProviderResult[] results = new ContentProviderResult[numOperations];
            for (int i = 0; i < numOperations; i++) {
                results[i] = operations.get(i).apply(this, results, i);
            }
            db.setTransactionSuccessful();
            return results;
        } finally {
            db.endTransaction();
        }
    }
}
