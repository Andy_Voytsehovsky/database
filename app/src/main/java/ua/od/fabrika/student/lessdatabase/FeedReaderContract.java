package ua.od.fabrika.student.lessdatabase;

import android.net.Uri;
import android.provider.BaseColumns;

import static ua.od.fabrika.student.lessdatabase.FeedReaderContract.FeedEntry.TABLE_NAME;

/**
 * Created by student on 6/3/2017.
 */

public class FeedReaderContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private FeedReaderContract() {}
    public static Uri CONTENT_URI = FeedReaderDbHelper.BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

    /*  Inner class that defines the table contents  */
    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_SUBTITLE = "subtitle";
    }

    static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    FeedEntry._ID + " INTEGER PRIMARY KEY," +
                    FeedEntry.COLUMN_NAME_TITLE + " TEXT," +
                    FeedEntry.COLUMN_NAME_SUBTITLE + " TEXT)";

    static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

}
