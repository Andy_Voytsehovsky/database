package ua.od.fabrika.student.lessdatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

/**
 * Created by student on 6/3/2017.
 */

public class DBHelper {
    FeedReaderDbHelper mDbHelper;

   public DBHelper(Context context){
        mDbHelper = FeedReaderDbHelper.getInstance(context);
    }
    public static Uri insertFeed(Context context, String title, String subtitle){
        ContentValues values = new ContentValues();
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE, title);
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE, subtitle);
        Uri uri = context.getContentResolver().insert(FeedReaderContract.CONTENT_URI, values);
        return uri;
    }

    public static Cursor selectFeed(Context context, String title){
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                FeedReaderContract.FeedEntry._ID,
                FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE,
                FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE
        };

        // Filter results WHERE "title" = 'My Title'
        String selection = FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE + " = ?";
        String[] selectionArgs = { title };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE + " DESC";
        Cursor cursor = context.getContentResolver().query(FeedReaderContract.CONTENT_URI, projection, selection, selectionArgs, sortOrder);
        return cursor;
    }

    public int deleteFeed(Context context, String title){
        // Define 'where' part of query.
        String selection = FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE + " LIKE ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = { title };
        // Issue SQL statement.
        int rowCount = context.getContentResolver().delete(FeedReaderContract.CONTENT_URI, selection, selectionArgs);
        return rowCount;
    }

    public static int updateFeed(Context context, String title, String subtitle){
        // New value for one column
        ContentValues values = new ContentValues();
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_SUBTITLE, subtitle);

        // Which row to update, based on the title
        String selection = FeedReaderContract.FeedEntry.COLUMN_NAME_TITLE + " LIKE ?";
        String[] selectionArgs = { title };

        int count = context.getContentResolver().update(FeedReaderContract.CONTENT_URI, values, selection, selectionArgs);
        return count;
    }
}
